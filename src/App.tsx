import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Container, CssBaseline } from "@mui/material";

import { Header } from "src/modules";

import WithTheme from "./components/WithTheme/WithTheme";
import Router from "./router/Router";

const App = () => {
  return (
    <BrowserRouter>
      <WithTheme>
        <CssBaseline />
        <Header />
        <Container maxWidth="lg">
          <Router />
        </Container>
      </WithTheme>
    </BrowserRouter>
  );
};

export default App;
