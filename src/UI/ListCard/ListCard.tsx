import React from "react";
import { Card, CardContent, Typography } from "@mui/material";

export type ListCardProps = {
  title: string;
  children: JSX.Element | JSX.Element[];
};

const ListCard: React.FC<ListCardProps> = ({ title, children }) => {
  return (
    <Card sx={{ mb: 1 }}>
      <CardContent>
        <Typography variant="h4">{title}</Typography>
        {children}
      </CardContent>
    </Card>
  );
};

export default ListCard;
