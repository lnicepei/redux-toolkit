import React from "react";
import { Link } from "react-router-dom";

type NavLinkProps = {
  to: string;
  children: JSX.Element | JSX.Element[] | string;
};

const NavLink: React.FC<NavLinkProps> = ({ to, children }) => {
  return (
    <Link to={to} style={{ textDecoration: "none" }}>
      {children}
    </Link>
  );
};

export default NavLink;
