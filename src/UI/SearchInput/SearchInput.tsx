import React from "react";
import { TextField, TextFieldProps } from "@mui/material";

const SearchInput: React.FC<TextFieldProps> = ({ ...rest }) => {
  return <TextField placeholder="Search..." {...rest} />;
};

export default SearchInput;
