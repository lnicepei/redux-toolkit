import React, { memo } from "react";
import { List } from "@mui/material";
import { Box, LinearProgress } from "@mui/material";
import { SerializedError } from "@reduxjs/toolkit";
import { FetchBaseQueryError } from "@reduxjs/toolkit/dist/query";

type CardListProps = {
  loading: boolean;
  cards: JSX.Element[];
  error: FetchBaseQueryError | SerializedError | undefined;
};

const CardList = memo(function CardList({
  error,
  cards,
  loading,
}: CardListProps) {
  return (
    <>
      {error ? (
        <>Something went wrong...</>
      ) : loading ? (
        <Box sx={{ width: "100%" }}>
          <LinearProgress />
        </Box>
      ) : (
        <List>{cards}</List>
      )}
    </>
  );
});

export default CardList;
