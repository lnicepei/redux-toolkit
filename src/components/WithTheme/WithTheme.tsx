import React from "react";
import { ThemeProvider } from "@mui/material";

import { useAppSelector } from "../../redux/hooks";

import { darkTheme, lightTheme } from "./themes/index";

type WithThemeProps = {
  children: JSX.Element | JSX.Element[];
};

const WithTheme: React.FC<WithThemeProps> = ({ children }) => {
  const theme = useAppSelector((state) => state.theme);

  return (
    <ThemeProvider theme={theme === "light" ? lightTheme : darkTheme}>
      {children}
    </ThemeProvider>
  );
};

export default WithTheme;
