import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import App from "src/App";
import { store } from "src/redux/store";

import "@testing-library/jest-dom";

describe("Loads the page", () => {
  it("Sets value for input", async () => {
    render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    const input = screen.getByPlaceholderText("Search...");

    await userEvent.click(input);
    await userEvent.type(input, "Aboba");

    expect(screen.getByDisplayValue("Aboba")).toBeInTheDocument();
    expect(input).toHaveFocus();
  });
});
