import React from "react";
import { useLocation } from "react-router-dom";
import { Box, Container, Typography } from "@mui/material";

import ThemeSwitcher from "src/modules/ThemeSwitcher/components/ThemeSwitcher/ThemeSwitcher";
import { useAppDispatch } from "src/redux/hooks";
import { NavLink, SearchInput } from "src/UI/index";

import { debounce } from "../../helpers/debounce";
import { setQuery } from "../../store/querySlice";

const Header = () => {
  const dispatch = useAppDispatch();
  const { pathname } = useLocation();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setQuery(e.target.value));
  };

  const debouncedChangeHandler = debounce(handleChange);

  return (
    <Container sx={{ gap: 1 }}>
      <Typography variant="h2" textAlign="center">
        Some {pathname.slice(1)} here!
      </Typography>
      <Box display="flex" mb={1}>
        <ThemeSwitcher />
        <SearchInput fullWidth onChange={debouncedChangeHandler} />
      </Box>
      <Box
        sx={{ display: "flex", justifyContent: "space-evenly", width: "100%" }}
      >
        <Typography variant="h5">
          <NavLink to="/posts">Posts</NavLink>
        </Typography>
        <Typography variant="h5">
          <NavLink to="/todos">Todos</NavLink>
        </Typography>
        <Typography variant="h5">
          <NavLink to="/news">News</NavLink>
        </Typography>
      </Box>
    </Container>
  );
};

export default Header;
