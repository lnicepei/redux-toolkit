/* eslint-disable @typescript-eslint/no-explicit-any */
export function debounce(fn: (...args: any[]) => any, ms = 500) {
  let timeoutId: ReturnType<typeof setTimeout>;
  return function (...args: any[]) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => fn(...args), ms);
  };
}
