import { createSlice } from "@reduxjs/toolkit";

import { RootState } from "src/redux/store";

const initialState = "";

const querySlice = createSlice({
  name: "search",
  initialState,
  reducers: {
    setQuery: (state, { payload }) => {
      return payload;
    },
  },
});

export const selectQuery = (state: RootState) => state.query;

export const { setQuery } = querySlice.actions;

export default querySlice.reducer;
