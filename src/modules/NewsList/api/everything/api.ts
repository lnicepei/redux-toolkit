import { newsApi } from "../api";

import { News, NewsResponse, QueryArgument } from "./types";

export const everythingApi = newsApi.injectEndpoints({
  endpoints: (build) => ({
    getNewsByName: build.query<News[], QueryArgument>({
      query: ({ query, sortBy }) => `/everything?q=${query}&sortBy=${sortBy}`,
      transformResponse: (data: NewsResponse) => data.articles,
    }),
  }),
  overrideExisting: false,
});

export const { useGetNewsByNameQuery } = everythingApi;
