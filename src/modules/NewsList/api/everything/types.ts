export interface News {
  title: string;
  author: string;
  description: string;
  publishedAt: string;
  urlToImage: string;
}

export interface NewsResponse {
  status: "ok" | "error";
  totalResults: number;
  articles: News[];
}

export interface QueryArgument {
  query: string;
  sortBy: string;
}
