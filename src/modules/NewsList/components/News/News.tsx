import React from "react";
import { Container } from "@mui/material";

import { ListCard } from "src/UI";

type NewsProps = {
  title: string;
  urlToImage: string;
};

const News: React.FC<NewsProps> = ({ title, urlToImage }) => {
  return (
    <ListCard title={title}>
      <Container>
        <img width={"100%"} src={urlToImage} alt={title} />
      </Container>
    </ListCard>
  );
};

export default News;
