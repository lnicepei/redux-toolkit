import React from "react";

import CardList from "src/components/CardList/CardList";
import { useAppSelector } from "src/redux/hooks";

import { useGetNewsByNameQuery } from "../../api/everything/api";
import News from "../News/News";

const NewsList = () => {
  const query = useAppSelector((state) => state.query);

  const { data, error, isLoading, isFetching } = useGetNewsByNameQuery({
    query: query,
    sortBy: "relevancy",
  });

  const res = data?.filter((news) => news.title?.includes(query)) ?? [];

  const cards = res.map((news, i) => (
    <News key={i} urlToImage={news.urlToImage} title={news.title} />
  ));

  return (
    <CardList error={error} loading={isLoading || isFetching} cards={cards} />
  );
};

export default NewsList;
