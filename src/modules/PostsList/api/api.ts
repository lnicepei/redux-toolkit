import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";

import { Post } from "./types";

export const postsApi = createApi({
  reducerPath: "posts",
  endpoints: (build) => ({
    getPosts: build.query<Post[], void>({
      query: () => `posts`,
    }),
  }),
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_POSTS_API,
  }),
});

export const { useGetPostsQuery } = postsApi;
