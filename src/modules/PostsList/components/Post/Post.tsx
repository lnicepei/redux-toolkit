import React from "react";
import { Typography } from "@mui/material";

import { ListCard } from "src/UI";

type PostProps = {
  title: string;
  body: string;
};

const Post: React.FC<PostProps> = ({ title, body }) => {
  return (
    <ListCard title={title}>
      <Typography variant="caption">{body}</Typography>
    </ListCard>
  );
};

export default Post;
