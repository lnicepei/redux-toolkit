import React from "react";

import CardList from "src/components/CardList/CardList";
import { useAppSelector } from "src/redux/hooks";

import { useGetPostsQuery } from "../../api/api";
import { Post as PostProps } from "../../api/types";
import Post from "../Post/Post";

const PostsList = () => {
  const query = useAppSelector((state) => state.query);
  const { data, error, isFetching, isLoading } = useGetPostsQuery();

  const res: PostProps[] =
    data?.filter((post) => post.title.includes(query)) ?? [];

  const loading = isFetching || isLoading;

  const cards = res.map((post) => (
    <Post key={post.id} title={post.title} body={post.body}></Post>
  ));

  return <CardList error={error} loading={loading} cards={cards} />;
};

export default PostsList;
