import React from "react";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import { Button } from "@mui/material";

import { useAppDispatch, useAppSelector } from "src/redux/hooks";

import { toggleTheme } from "../../store/themeSlice";

const ThemeSwitcher = () => {
  const dispatch = useAppDispatch();
  const theme = useAppSelector((state) => state.theme);

  const handleToggle = () => {
    dispatch(toggleTheme());
  };

  return (
    <Button onClick={handleToggle}>
      {theme === "light" ? <Brightness4Icon /> : <Brightness7Icon />}
    </Button>
  );
};

export default ThemeSwitcher;
