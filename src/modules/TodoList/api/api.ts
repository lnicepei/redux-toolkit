import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";

import { Todo } from "./types";

export const todoListApi = createApi({
  reducerPath: "todoList",
  endpoints: (builder) => ({
    getTodoById: builder.query<Todo, string>({
      query: (id: string) => `/todos/${id}`,
    }),
    getAllTodos: builder.query<Todo[], void>({
      query: () => `/todos/`,
    }),
    toggleTodo: builder.mutation<Todo, Partial<Todo> & Pick<Todo, "id">>({
      query: ({ id, completed }) => ({
        url: `/todos/${id}`,
        method: "PATCH",
        body: JSON.stringify({
          id,
          completed,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
        mode: "cors",
      }),
    }),
  }),
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_TODOS_API,
  }),
});

export const {
  useGetTodoByIdQuery,
  useGetAllTodosQuery,
  useToggleTodoMutation,
} = todoListApi;
