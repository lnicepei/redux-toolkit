import React, { useState } from "react";
import { Box, Checkbox, CircularProgress } from "@mui/material";

import { ListCard } from "src/UI";

import { useToggleTodoMutation } from "../../api/api";

type TodoProps = {
  id: number;
  title: string;
  completed: boolean;
};

const Loader = (
  <CircularProgress
    size={50}
    sx={{
      position: "absolute",
      top: -4,
      left: -4,
    }}
  />
);

const Todo: React.FC<TodoProps> = ({ title, completed, id }) => {
  const [toggleTodo] = useToggleTodoMutation();
  const [localCompleted, setLocalCompleted] = useState(() => completed);
  const [isLoading, setIsLoading] = useState(false);

  const handleToggleComplete = () => {
    setIsLoading(true);

    toggleTodo({ id, completed }).then(() => {
      setLocalCompleted((p) => !p);
      setIsLoading(false);
    });
  };

  return (
    <ListCard title={title}>
      <Box sx={{ m: 1, position: "relative" }}>
        <Checkbox checked={localCompleted} onClick={handleToggleComplete} />
        {isLoading && Loader}
      </Box>
    </ListCard>
  );
};

export default Todo;
