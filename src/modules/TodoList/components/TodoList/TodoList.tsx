import React from "react";

import CardList from "src/components/CardList/CardList";
import { useAppSelector } from "src/redux/hooks";

import { useGetAllTodosQuery } from "../../api/api";
import { Todo as TodoType } from "../../api/types";
import Todo from "../Todo/Todo";

const TodoList = () => {
  const query = useAppSelector((state) => state.query);
  const { data, error, isLoading, isFetching } = useGetAllTodosQuery();

  const res: TodoType[] =
    data?.filter((todo) => todo.title.includes(query)) ?? [];

  const cards = res.map((todo) => (
    <Todo
      id={todo.id}
      key={todo.id}
      title={todo.title}
      completed={todo.completed}
    />
  ));

  const loading = isLoading || isFetching;

  return <CardList loading={loading} error={error} cards={cards} />;
};

export default TodoList;
