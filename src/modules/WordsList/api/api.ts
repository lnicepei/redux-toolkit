import {} from "@reduxjs/toolkit/query/react";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

import { Word } from "./types";

export const dictionaryApi = createApi({
  reducerPath: "dictionaryApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_DICTIONARY_API,
  }),
  endpoints: (build) => ({
    getWordDetails: build.query<Word[], string>({
      query: (word: string) => `/entries/en/${word}`,
    }),
  }),
});

export const { useGetWordDetailsQuery } = dictionaryApi;
