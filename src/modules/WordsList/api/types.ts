export interface Word {
  word: string;
  phonetic: string;
  phonetics: Phonetics[];
  meanings: Meaning[];
  license: License[];
  sourceUrls: string[];
}

export interface Phonetics {
  text: string;
  audio: string;
  sourceUrl: string;
  license: {
    name: string;
    url: string;
  };
}

export interface Meaning {
  partOfSpeech: "noun" | "verb" | "adverb" | "pronoun" | "interjection";
  definitions: Definition[];
  synonyms: string[];
  antonyms: string[];
}

interface Definition {
  definition: string;
  synonyms: string[];
  antonyms: string[];
}

interface License {
  name: string;
  url: string;
}
