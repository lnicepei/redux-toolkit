import React from "react";
import { Button, Typography } from "@mui/material";

import ListCard from "src/UI/ListCard/ListCard";

import { Meaning, Phonetics } from "../../api/types";

import useAudio from "./hooks/useAudio";

type WordProps = {
  word: string;
  meanings: Meaning[];
  audio: Phonetics;
  phonetic: string;
};

const Word: React.FC<WordProps> = ({ word, meanings, audio, phonetic }) => {
  const { isPlaying, togglePlaying } = useAudio(audio?.audio);

  const jsxMeanings = meanings.map((meaning, i) => (
    <React.Fragment key={i}>
      <Typography sx={{ mb: 1.5 }} color="text.secondary">
        {meaning.partOfSpeech}
      </Typography>
      {meaning.definitions.map((d, j) => (
        <Typography key={j}>{d.definition}</Typography>
      ))}
    </React.Fragment>
  ));

  return (
    <ListCard title={word}>
      <Typography>{phonetic}</Typography>
      <Button disabled={isPlaying} onClick={togglePlaying}>
        Play Sound
      </Button>
      <>{jsxMeanings}</>
    </ListCard>
  );
};

export default Word;
