import React from "react";

import CardList from "src/components/CardList/CardList";
import { useAppSelector } from "src/redux/hooks";

import { useGetWordDetailsQuery } from "../../api/api";
import { Word as WordType } from "../../api/types";
import Word from "../Word/Word";

const WordsList = () => {
  const query = useAppSelector((state) => state.query);
  const { data, error, isLoading, isFetching } = useGetWordDetailsQuery(query);

  const res: WordType[] = data ?? [];

  const loading = isLoading || isFetching;

  const cards = res.map((word, i) => (
    <Word
      key={i}
      word={word.word}
      phonetic={word.phonetic}
      meanings={word.meanings}
      audio={word.phonetics.filter((phonetic) => phonetic.audio)[0]}
    />
  ));

  return <CardList loading={loading} error={error} cards={cards} />;
};

export default WordsList;
