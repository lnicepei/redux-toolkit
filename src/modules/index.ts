export { Header, queryReducer } from "./Header";
export { newsApi, NewsList } from "./NewsList";
export { postsApi, PostsList } from "./PostsList";
export { themeReducer, ThemeSwitcher } from "./ThemeSwitcher";
export { TodoList, todoListApi } from "./TodoList";
export { dictionaryApi, WordsList } from "./WordsList";
