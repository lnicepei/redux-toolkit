import { combineReducers, configureStore, Reducer } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";

import {
  dictionaryApi,
  newsApi,
  postsApi,
  queryReducer,
  themeReducer,
  todoListApi,
} from "src/modules";

import { RESET_STATE_ACTION_TYPE } from "./actions/resetState";

const reducers = {
  [newsApi.reducerPath]: newsApi.reducer,
  [todoListApi.reducerPath]: todoListApi.reducer,
  [dictionaryApi.reducerPath]: dictionaryApi.reducer,
  [postsApi.reducerPath]: postsApi.reducer,
  query: queryReducer,
  theme: themeReducer,
};

const combinedReducer = combineReducers(reducers);

export const rootReducer: Reducer<RootState> = (state, action) => {
  if (action.type === RESET_STATE_ACTION_TYPE) {
    state = {} as RootState;
  }

  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: (gDM) =>
    gDM().concat(
      todoListApi.middleware,
      newsApi.middleware,
      dictionaryApi.middleware,
      postsApi.middleware
    ),
});

setupListeners(store.dispatch);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof combinedReducer>;
