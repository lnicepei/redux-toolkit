import React from "react";
import { Routes } from "react-router";
import { Navigate, Route } from "react-router-dom";

import { NewsList, PostsList, TodoList } from "../modules";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="posts" />} />
      <Route path="todos" element={<TodoList />} />
      <Route path="posts" element={<PostsList />} />
      <Route path="news" element={<NewsList />} />
    </Routes>
  );
};

export default Router;
